import React, { Component } from "react";

import "./styles.css";

export default class SignupPage extends Component {
  constructor(props) {
    super(props);
    this.emailElement = React.createRef();
    this.passwordElement = React.createRef();
  }

  validateInputData = (email, password) => {
    if (email.trim().length === 0 || password.trim().length === 0) {
      return false;
    }

    return true;
  };

  signup = async event => {
    event.preventDefault();

    const email = this.emailElement.current.value;
    const password = this.passwordElement.current.value;

    if (!this.validateInputData(email, password)) {
      return;
    }

    const requestBody = {
      query: `
        mutation {
          createUser(userInput: {email: "${email}", password: "${password}"}) {
            _id email
          }
        }
      `
    };

    this.send(requestBody);
  };

  login = async event => {
    event.preventDefault();

    const email = this.emailElement.current.value;
    const password = this.passwordElement.current.value;

    if (!this.validateInputData(email, password)) {
      return;
    }

    const requestBody = {
      query: `
        query {
          login(email: "${email}", password: "${password}") {
            userId token tokenExpiration
          }
        }
      `
    };

    this.send(requestBody);
  };

  send = async requestBody => {
    try {
      const response = await fetch("http://localhost:8000/graphql", {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: {
          "Content-Type": "application/json"
        }
      });

      if (response.status !== 200 && response.status !== 201) {
        throw new Error("Failed!");
      }

      const data = await response.json();

      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <form className="login-form" onSubmit={this.signup}>
        <div className="form-control">
          <label htmlFor="email">E-mail</label>
          <input type="email" id="email" ref={this.emailElement} />
        </div>
        <div className="form-control">
          <label htmlFor="password">Password</label>
          <input type="password" id="password" ref={this.passwordElement} />
        </div>
        <div className="form-actions">
          <button type="button" onClick={this.login}>
            Login
          </button>
          <button type="submit">Sign Up</button>
        </div>
      </form>
    );
  }
}
