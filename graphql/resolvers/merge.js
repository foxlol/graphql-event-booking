const Event = require('../../models/event');
const User = require('../../models/user');
const { dateToISOString } = require('../../helpers/date');

const transformEvent = event => {
  return { 
    ...event._doc,
    _id: event.id,
    date: dateToISOString(event._doc.date),
    creator: fetchUser.bind(this, event.creator)
  };
};

const transformBooking = booking => {
  return { 
    ...booking._doc, 
    _id: booking.id, 
    event: fetchEvent.bind(this, booking._doc.event),
    user: fetchUser.bind(this, booking._doc.user),
    createdAt: dateToISOString(booking._doc.createdAt),
    updatedAt: dateToISOString(booking._doc.updatedAt)
  };
};

const fetchEvents = async eventsIds => {
  try {
    const events = await Event.find({_id: {$in: eventsIds}});

    return events.map(event => {
      return transformEvent(event);
    });
  } catch(err) {
    throw err;
  }
};

const fetchUser = async userId => {
  try {
    const user = await User.findById(userId);

    return { 
      ...user._doc, 
      _id: user.id,
      createdEvents: fetchEvents.bind(this, user._doc.createdEvents) 
    };
  } catch(err) {
    throw err;
  }
};

const fetchEvent = async eventId => {
  try {
    const event = await Event.findById(eventId);
    
    return transformEvent(event);
  } catch (err) {
    throw err;
  }
};

module.exports = { 
  transformBooking, 
  transformEvent 
};