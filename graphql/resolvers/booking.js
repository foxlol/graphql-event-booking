const Event = require('../../models/event');
const Booking = require('../../models/booking');
const { transformEvent, transformBooking } = require('./merge');

module.exports = {
  bookings: async (args, req) => {
    try {
      if (!req.isAuthenticated) {
        throw new Error('User is not authenticated');
      }

      const bookings = await Booking.find();

      return bookings.map(booking => {
        return transformBooking(booking);
      });
    } catch (err) {
      throw err;
    }
  },
  bookEvent: async (args, req) => {
    try {
      if (!req.isAuthenticated) {
        throw new Error('User is not authenticated');
      }

      const fetchedEvent = await Event.findById(args.eventId);

      if (!fetchedEvent) {
        throw new Error('Event not found');
      }

      const booking = new Booking({
        event: fetchedEvent,
        user: req.userId
      });

      await booking.save();

      return transformBooking(booking);
    } catch (err) {
      throw err;
    }
  },
  cancelBooking: async (args, req) => {
    try {
      if (!req.isAuthenticated) {
        throw new Error('User is not authenticated');
      }

      const { event } = await Booking.findById(args.bookingId).populate('event');
      const bookedEvent = transformEvent(event);

      await Booking.deleteOne({ _id: args.bookingId });

      return bookedEvent;
    } catch (err) {
      throw err;
    }
  }
};