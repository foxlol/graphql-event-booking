const Event = require('../../models/event');
const User = require('../../models/user');
const { transformEvent } = require('./merge');

module.exports = {
  events: async () => {
    try {
      const events = await Event.find();

      return events.map(event => {
        return transformEvent(event);
      });
    } catch(err) {
      throw err;
    }
  },
  createEvent: async ({ eventInput }, req) => {
    try {
      if (!req.isAuthenticated) {
        throw new Error('User is not authenticated');
      }
      
      const event = new Event({
        title: eventInput.title,
        description: eventInput.description,
        price: +eventInput.price,
        date: new Date(eventInput.date),
        creator: req.userId
      });

      let createdEvent = await event.save();

      const creator = await User.findById(createdEvent.creator._id);

      if (!creator) {
        throw new Error('User does not exist');
      }

      creator.createdEvents.push(createdEvent);

      await creator.save();

      return transformEvent(createdEvent);
    } catch(err) {
      throw err;
    }
  }
};