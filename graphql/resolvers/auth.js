const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
  createUser: async args => {
    const existingUser = 
      await User.findOne({email: args.userInput.email});

    if (existingUser) {
      throw new Error('User exists already');
    }

    const hashedPassword = 
      await bcrypt.hash(args.userInput.password, 12);

    const user = new User({
      email: args.userInput.email,
      password: hashedPassword
    });

    await user.save();

    return { ...user._doc, password: null, _id: user.id };
  },
  login: async ({ email, password }) => {
    try {
      const user = await User.findOne({ email });

      if (!user) {
        throw new Error('Invalid credentials');
      }

      const isEqual = await bcrypt.compare(password, user.password);

      if (!isEqual) {
        throw new Error('Invalid credentials');
      }

      const token = jwt.sign(
        { userId: user.id, email: user.email },
        process.env.JWT_KEY,
        { expiresIn: '1h' }
      );

      return {
        userId: user.id,
        token,
        tokenExpiration: 1
      };
    } catch (err) {
      throw err;
    }
  }
};